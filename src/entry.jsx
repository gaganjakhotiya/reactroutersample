var React = require('react');
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');
var createHistory = require('history').createHistory;

var Router = ReactRouter.Router;
var Route = ReactRouter.Route;

var Prod = require('./App.jsx');
var App = Prod.App;
var Message = Prod.Message;
var Exit = Prod.Exit;

var routes = (
	<Route path="/" component={App}>
		<Route path="message" component={Message}/>
		<Route path="exit" component={Exit}/>
	</Route>
);

ReactDOM.render(<Router history={createHistory()}>{routes}</Router>, document.getElementById('content')) 