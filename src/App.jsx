var React = require('react');
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');

var RouterHandler = ReactRouter.RouteHandler;
var Link = ReactRouter.Link;

var Message = React.createClass({
	render: function(){
		return (
			<div>
				<h1>Hello</h1>
				<p>World</p>
			</div>
		);
	}
});

var Exit = React.createClass({
	render: function(){
		return (
			<div>
				<h1>Thank You</h1>
				<p>Bye bye</p>
			</div>
		);
	}
});

var App = React.createClass({
	render: function(){
		var styles = {
			height: '300px',
			width: '100%',
			padding: '15px',
			background: '#eee'
		}

		return (
			<div>
				<h1>App</h1>
				<ul>
					<li><Link to="/">Home</Link></li>
					<li><Link to="message">Message</Link></li>
					<li><Link to="exit">Exit</Link></li>
				</ul>
				<div id="routes-div" style={styles}>
					{this.props.children}
				</div>
			</div>
		);
	}
});

module.exports = {
	App:App,
	Message:Message,
	Exit:Exit
};