var webpack = require('webpack');
module.exports = {
    entry: "./src/entry.jsx",
    output: {
        publicPath: "/",
        path: "./",
        filename: "bundle.js"
    },
    resolve: {
    	extensions: ['', '.js', '.jsx']
    },
    plugins: [
        //new webpack.optimize.UglifyJsPlugin()
    ],
    module: {
    	loaders: [
            { test: /\.jsx$/, loader: 'jsx-loader'}
    	]
    }
};